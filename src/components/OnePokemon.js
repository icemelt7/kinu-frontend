import React, {useEffect, useState} from 'react';

function OnePokemon(props) {
    var [result, setResult] = useState(null);
    useEffect(() => {
        fetch(`https://pokeapi.co/api/v2/pokemon/${props.id}/`).then(raw => {
            raw.json().then(res => {
                setResult(res);
            })
        })
    }, []);

    return (
    <div className="App">
        {result ? <img width="300px" src={result.sprites.front_default} /> : <p>Loading</p>}
    </div>
    );
}

export default OnePokemon;