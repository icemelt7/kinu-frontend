import React, { useState, useEffect } from 'react';
import { Link } from "@reach/router"
function ListOfPokemon(props) {
   
    var [result, setResult] = useState(null);
    useEffect(() => {
        fetch('https://pokeapi.co/api/v2/pokemon/?offset=20&limit=20').then(raw => {
            raw.json().then(res => {
                setResult(res);
            })
        })
    }, []);

    return <div>
      {result ? <div>{
          result.results.map(p => <Pokemon key={p.name} p={p} />)
      }</div> : <p>Loading</p>}
    </div>
  }

  function Pokemon(props) {
    var blue = {
        color: 'blue'
    }
    
    
    return <li style={blue}>
        <Link to={`/pokemon/${props.p.name}/${props.p.url.split("/")[6]}`}>
            {props.p.name}
        </Link>
    </li>
  }
export default ListOfPokemon;