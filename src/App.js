import React, { useState } from 'react';
import { Router } from "@reach/router"
import logo from './logo.svg';
import './App.css';
import ListOfPokemon from './components/List';
import OnePokemon from './components/OnePokemon';

function App() {
  return (
    <div className="App">
     
      {/* <Router>
        <ListOfPokemon path="/" />
        <OnePokemon path="/pokemon/:name/:id" />
      </Router> */}

      {/* <Sandwich /> */}
      <Form />
    </div>
  );
}

function Form() {
  var [username, setUsername] = useState('')
  const onSubmit = (e) => {
    e.preventDefault();
    
  }
  return <form onSubmit={onSubmit}>
    <input name="username" value={username} onChange={ (e) => setUsername(e.target.value) }/>
  </form>
}

function Sandwich(props) {
  const [salt, setSalt] = useState(false)
  return <div>I am sandwich: <Tomato setSalt={setSalt} salt={salt} /></div>
}

function Tomato(props) {
  const addSalt = () => {
    props.setSalt(true);
  }
  return <div>Tomato <button onClick={addSalt}>Add Salt</button> Salt included: {props.salt ? 'yes' : 'no' } </div>
}

export default App;